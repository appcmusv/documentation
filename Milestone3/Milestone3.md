#Milestone 3

##Due Thursday, November 17, 2016


##Scope

Fully implement the Encryption, Authentication and Access Control as specified in the  
Specification document file for Milestone 3 in the same folder. 

## Submission

**Make sure you complete the following two submissions**

Submit your code by sharing your Bitbucket repository with the Instructor and Teaching Assistant.

Submit a one page status report with the following information

+ Contributions of from individual members of the team
+ How many hours did each team member spend on the work this week
+ Challenges that you faced or risks that you foresee for yourself in completing the project.

## Grading Criteria

| Component | Weight |
|-----|------:|
| Password Security | 10
| Get Methods fixed | 10
| Drivers and Passengers modified to avoid Duplicate Email | 10
| Require Authentication for Specified Methods | 30
| Sessions resource implemented | 10
| Sessions token implemented correctly | 15
| Individual Contribution | 10
| Status report | 5


