# Security Implementation

In this milestone, you will implement encryption of key data in the system, 
add authentication and provide access control by implementing two roles
for users. 

## Encryption/Hashing Data

Study and compare __encryption__ and __hashing__ by reading about them on the web.
Below are some useful links for this.

+ [Wikipedia - Encryption](https://en.wikipedia.org/wiki/Encryption)
+ [Secured Password Hashing](http://security.blogoverflow.com/2013/09/about-secure-password-hashing/)
+ [Encryption vs Hashing for Passwords](http://www.darkreading.com/safely-storing-user-passwords-hashing-vs-encrypting/a/d-id/1269374)
+ [Salted Password Hashing](https://crackstation.net/hashing-security.htm)

Once done, do the following to complete this port of the Milestone.

+ Identify a mechanism for storing password that you think works for you. You can also leverage the technique
discussed in the APP class. Create a brief description in your submission document.
+ Implement this mechanism for storing the password. Your `POST` method must accept a string
password, apply  the encryption/hashing mechanism and store the resulting
value. 
+ Change your `GET` methods for `passengers` and `drivers` so that the password
field is not available in the returned response body.


## Authentication

Modify your code to do the following

+ Implement `sessions` API described below. Both drivers and passengers authenticate
with this API. You will know who they are by first testing them against 
drivers and then against passengers.


## Access Control

Implement the following

+ Modify `POST` methods for `drivers` and `passengers` so that you cannot have
a driver and a passenger with the same email address. 
+ Require authentication before `POST` method is called `rides` and `cars`, including `POST` for `cars` within the 
`drivers` resource. Only drivers can call `POST` method on `cars` and, either passengers or drivers can call
`POST` methods on `rides`. For now, you don't have to restrict that a driver can only
create cars for themselves.


## Sessions API

### POST /sessions

Authenticate a user with the following request body

    {
        "email": "john@doe.com",
        "password" : "somepassword"
    }
    
Authenticates the user against the driver and passenger data and returns the following response body

    {
        "token" : "787897dasd78d7f9s7df98sdf89sd7f89sd7f89sdf"
    }
    
    
The token should be hashed and encrypted, and contain  the user's ID and an expiration date.
