#Milestone 1

##Due Thursday, November 3, 2016


##Scope

Fully implement the API specified in the API Specification document file Milestone 1.

## Submission

**Make sure you complete the following two submissions**

Submit your code by sharing your Bitbucket repository with the Instructor and Teaching Assistant.

Submit a one page status report with the following information

+ Contributions of from individual members of the team
+ How many hours did each team member spend on the work this week
+ Challenges that you faced or risks that you foresee for yourself in completing the project.

## Grading Criteria

| Component | Weight |
|-----|------:|
| All End Points Functionally Implemented | 60 
| Error Handling Implemented | 10 
| Comments for All End Points | 10
| Variable and Functiona Naming Conventions | 5 
| Individual Contribution | 10
| Status report | 5

