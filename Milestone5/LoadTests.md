# Scale Testing your API
 
In this milestone, you will do load testing on the APIs that you created in the
previous milestones. 

+ Create a a GCE instance of the smallest size possible (You can start with `n1-standard-1`)
+ Run load tests on your API. In order to implement this, you will need to create a load test with a common 
 use case (see below)
+ Initiate this use case at the rate of 100 instances every 1 minute, and then repeat by halving the interval (30 seonds, 15 seconds etc.)
+ Record the response times and plot them.
+ Determine the load at which the response time starts to increase non-lineraly


## Use Case for Load Testing
 
+ Create a driver
+ Create a car for the driver
+ Create a passenger
+ Create a ride for the passenger
+ Update the ride with the created Driver and a Car
+ Add route points every 10 seconds for a 2 minute ride
+ Mark the ride as complete
+ Repeat by creating 10 rides for the same passenger, car and driver combination.


## References


