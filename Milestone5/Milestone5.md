#Milestone 5

##Due Monday, December 5, 2016


##Scope

Do load testing on your API per instructions in Milestone 5 document

## Submission

**Make sure you complete the following two submissions**

Submit your code by sharing your Bitbucket repository with the Instructor and Teaching Assistant.

Submit a one page status report with the following information

+ Contributions of from individual members of the team
+ How many hours did each team member spend on the work this week
+ Challenges that you faced or risks that you foresee for yourself in completing the project.

## Grading Criteria

| Component | Weight |
|-----|------:|
| Create jMeter Plans for each existing callback | 65
| Serve the existing API and DB in a Google Compute Engine Instance | 20
| Individual Contribution | 10
| Status report | 5


