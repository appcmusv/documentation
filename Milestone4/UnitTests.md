# Unit Test Implementation

In this milestone, you will implement unit tests for all APIs that you created in the
previous milestones. Coverage of ALL APIs is required for full credit. 

## References
[Spark Unit Testing](https://sparktutorials.github.io/2015/07/30/spark-testing-unit.html)

