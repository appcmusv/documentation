#Milestone 4

##Due Thursday, November 24, 2016


##Scope

Fully implement unit tests providing full coverage for all APIs. 

## Submission

**Make sure you complete the following two submissions**

Submit your code by sharing your Bitbucket repository with the Instructor and Teaching Assistant.

Submit a one page status report with the following information

+ Contributions of from individual members of the team
+ How many hours did each team member spend on the work this week
+ Challenges that you faced or risks that you foresee for yourself in completing the project.

## Grading Criteria

| Component | Weight |
|-----|------:|
| Coverage for Drivers | 20
| Coverage for Passengers | 15
| Coverage for Cars| 15
| Coverage for Rides| 25
| Coverage for Sessions| 10
| Individual Contribution | 10
| Status report | 5


