# API Enhancements for Milestone 2


## Drivers

The following new API end points will allow cars to be created and managed in the context of a driver.
Other actions like `GET` on a single car, `PUT` and `DELETE` will use the `/cars` end point. 

###  GET /drivers/:driverId/cars

Returns the list of all Cars associated with the driver in the system. 

### POST /drivers/:driverId/cars

Creates a new car in the system associated with the specified driver whose ID is given. 
The car is created from the object that is passed to it. All fields in the body below 
are required. This call returns
an modified JSON containing the internally generated `id` and other properties populated by the system.

On failure, this will generate a `400 Bad Request` with a description of the failure.

#### Request Body

     {
	   "make": String(50),
	   "model": String(50),
       ...
       ...
      }
      

### GET /drivers/:driverId/rides

Returns a list of rides associated with the passenger. 
      
## Passengers

### GET /passegers/:passengerId/rides

Returns a list of rides associated with the passenger. 

## Cars

You will delete the following methods (Note: You can repurpose this code for other parts of this milestone)

+ `POST /cars` since you can no longer create cars that are not associated witha driver

###  GET /cars/:carId/drivers

Returns the a single element array containing the driver associated with the car
 
## Rides


### Single Ride Enhancement

Enhance the ride request object to allow for creation with additional references. 


	{
        "id": GUID,
        "rideType": String(16) // Values are ECONOMY, PREMIUM, EXECUTIVE
        "startPoint" : { lat: Decimal, long: Decimal }
        "endPoint": { lat: Decimal, long: Decimal }
        "requestTime" : Number (Timestamp)
        "pickupTime" : Number  (TimeStamp)
        "dropOffTime" : Number, (TimeStamp)
        "status" : String [REQUESTED, AWAITING_DRIVER, DRIVE_ASSIGNED, IN_PROGRESS, ARRIVED, CLOSED]
        "fare": Decimal,
        "driverId" : GUID of the driver,
        "carId" : GUID of the car,
        "passengerId" : GUID of the passenger
	}



### POST /rides/:rideId/routePoints

Creates a  route point on the route taken for the ride. This allows a client to set a series of points on
the route that the driver takes for a given ride. 

    {
        timestamp: Number (TImestamp)
        lat: Decimal,
        long: Decimal
    }

### GET /rides/:rideId/routePoints

Gets the complete list of route points ordered by timestamp (ascending order)

### GET /rides/:rideId/routepoints/latest

Gets the latest (by timestamp) single route point


