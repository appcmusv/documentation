#Milestone 2

##Due Thursday, November 10, 2016


##Scope

Fully implement the API specified in the API Specification document file for Milestone 2. This API
improves on the API specification from Milestone 1 by adding  resource references and sub-resources. 

## Submission

**Make sure you complete the following two submissions**

Submit your code by sharing your Bitbucket repository with the Instructor and Teaching Assistant.

Submit a one page status report with the following information

+ Contributions of from individual members of the team
+ How many hours did each team member spend on the work this week
+ Challenges that you faced or risks that you foresee for yourself in completing the project.

## Grading Criteria

| Component | Weight |
|-----|------:|
| All New End Points Functionally Implemented | 40
| Enhancements to existing end points fully implemented | 20
| Error Handling Implemented | 10 
| Comments for All End Points | 10
| Variable and Functiona Naming Conventions | 5 
| Individual Contribution | 10
| Status report | 5


