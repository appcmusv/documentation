#APP 2 - Independent Study


##Instructions

Documentation is now arranged by Milestones. **Look under "Milestone1" folder for the API specs for Milestone 1.**

## Weekly Activities

| Week | Activity | 
|------|----|
|1	| Prepare Environment |
|2	| Implement Milestone 1 API |
|3	| Implement Milestone 2 API	|
|4	| Implement Encryption, Authentication & Access Control and Unit Tests (Milestone 3)|
|5 	| Complete Unit Tests for Full Coverage (Milestone 4)|
|6 | Test Scaling (Milestone 5)|
|7 	| Test Performance (Mileston 6)|
| 8 | Test Reliability (Mielstone 7)|

Each week officially ends at the meeting time with the instructor. 

## Meeting Times

Meeting times with the instructor are scheduled as below

|Date| Day | Time |
|----:|:----|-----:|
|October 28| Friday| 1:30 pm
| November 3| Thursday| 1:30 pm
| November 10 | Thursday | 1:30 pm
| November 17 | Thursday | 1:30 pm
| November 23 | Wednesday | TBD
| December 1 | Thursday | 1:30 pm
| December 8	| Thursday | 1:30 pm
| December 15 | Thursday | 1:30 pm


## Grading Criteria

Each of the severn milestones are graded independently and equally. The resulting score will determine the grade.


Points Earned | Letter Grade
|----:|:-----:|
94 – 100 | A
90 – 93 | A-
87 – 89 | B+
84 – 86 | B
80 – 83 | B-
77 – 79 | C+
74 – 76 | C
70 – 73 | C-
60 – 69 | D

## Late Submissions

For each day your submission is late, your credit will be deducted as below.

+ One day late: 10% (_0% for Milestone 1_)
+ Two days late: 20% (_0% for Milestone 1_)
+ Three days late: 30% (_0% for Milestone 1_)
+ Four to seven days late: 50% (_10% for Milestones 1_)
+ More than seven days late: No credit (_all Milestones_)

## Technology
	
Following ar the preferred technologies for the Independent Study. If you choose to use different technologies, you will be on our own in terms of obtaining support for using that technology. 

+ Spark
+ Jackson
+ MongoLink
